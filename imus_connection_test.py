# Script testing the connection with the IMUs
# and printing the quaternion readings

import bluetooth
# the witmotion parsing pip package
import pywitmotion as wit
import threading

# address of the IMUs
imu_addr = [
    "00:0C:BF:16:71:33", # 1
    "00:0C:BF:09:46:4B", # 2
    "00:0C:BF:02:1E:40", # 3
    "00:0C:BF:02:3B:42"  # 4
]

# a list containing all quaternion readings
q = [[] for i in imu_addr]

# a callback method for each IMU
def read_from_soc(soc,ind,):
    global q
    data = soc.recv(36)
    while True:
        data = soc.recv(36)
        for msg in reversed(data.split(b'U')):
            q1 = wit.get_quaternion(msg)
            if q1 is not None:
                q[ind]=q1
                break
                
# run the threads and, one for each IMU               
sockets = []
threads = []
for n, imu in enumerate(imu_addr):
    s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    try:
        s.connect((imu, 1))
        print("Device {} - {} connected!". format(n,imu))
    except :
        print("Device {} - {} not available!". format(n,imu))
        continue;
    sockets.append(s)
    t = threading.Thread(target=read_from_soc, args=(s,n,))
    t.start()
    threads.append(t)

# print the quaternion readings
import time
while True:
    print("Quaternions:")
    print(q)
    time.sleep(1)
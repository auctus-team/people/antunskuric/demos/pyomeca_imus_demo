import bluetooth
# the witmotion parsing pip package
import pywitmotion as wit
import threading
import time

# address of the IMUs
imu_addr = [
    "00:0C:BF:16:71:33", # 1
    "00:0C:BF:09:46:4B", # 2
    "00:0C:BF:02:1E:40", # 3
    "00:0C:BF:02:3B:42"  # 4
]

# a list containing all quaternion readings
q = [[] for i in imu_addr]

# a callback method for each IMU
def read_from_soc(soc,ind,):
    global q
    data = soc.recv(36)
    while True:
        data = soc.recv(36)
        for msg in reversed(data.split(b'U')):
            q1 = wit.get_quaternion(msg)
            if q1 is not None:
                q[ind]=q1
                break
                
# run the threads and, one for each IMU               
sockets = []
threads = []
for n, imu in enumerate(imu_addr):
    s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    try:
        s.connect((imu, 1))
        print("Device {} - {} connected!". format(n,imu))
    except :
        print("Device {} - {} not available!". format(n,imu))
        continue;
    sockets.append(s)
    t = threading.Thread(target=read_from_soc, args=(s,n,))
    t.start()
    threads.append(t)

# ------------------------------------------------------------------------
# run the inverse kinematics with pyomeca biorbd and visualise with bioviz
# ------------------------------------------------------------------------

import numpy as np
# biomechanics software pyomeca biorbd and bioviz
import biorbd
import bioviz
from bioviz.biorbd_vtk import VtkModel, VtkWindow, Mesh
# scipy to handle quaternions
from scipy.spatial.transform import Rotation as R
# polytope library
import pycapacity

# Load a predefined model
model = biorbd.Model("pyomeca_models/MOBL_ARMS_fixed_33_movingbase.bioMod")
nq = model.nbQ()
nb_mus = model.nbMuscles()

# get min and max joint angles
Q_min = []
Q_max= []
for s in range(model.nbSegment()):
    for i in model.segment(s).QRanges():
        Q_min.append(i.min())
        Q_max.append(i.max())
Q_min = np.array(Q_min)
Q_max = np.array(Q_max)

# inital configuration - first kalman step
qinit = np.array([ -0.04748828,  1.04941699,  0.11751443,  1.10589953, -0.06330007,
        0.42163834,  0.057855 ])

# the generalised coordinates of the model
Q = biorbd.GeneralizedCoordinates(qinit)
Qdot = biorbd.GeneralizedVelocity(model)
Qddot = biorbd.GeneralizedAcceleration(model)

# Create a Kalman filter structure
freq = 1000  # Hz
params = biorbd.KalmanParam(freq)
# kalman = biorbd.KalmanReconsMarkers(model, params)
kalman = biorbd.KalmanReconsIMU(model, params)

# function normalising an angle between -pi and pi
# trying to keep it in the joint limits as well
# its not a hard limit
#
# @param angles: np.array or scalr - joint angles
# @param q_min: np.array or scalr - joint angles min
# @param q_max: np.array or scalr - joint angles max
def normalize(angles, q_min, q_max): 
    norm_angle = angles - np.floor(angles/(2*np.pi)+0.5)*2*np.pi
    norm_angle[norm_angle > q_max] -= 2*np.pi 
    norm_angle[norm_angle < q_min] += 2*np.pi
    return norm_angle

# bioviz viewer intialisation 
# many parameters, mostly to disable stuff that are visualised by default
# they are changing a lot so you might need to add new parameters with new versions
b = bioviz.Viz(loaded_model=model,
               background_color=(1,1,1), 
               show_gravity_vector=False,
               show_floor=False,
               show_local_ref_frame=False, 
               show_global_ref_frame=False, 
               show_markers=False,
               show_global_center_of_mass=False,
               show_segments_center_of_mass=False, 
               show_wrappings=False)
# define the meshes for the polytope - without robot
vtkMeshView = VtkModel(b.vtk_window, patch_color=[[0,0.5,0.8]],mesh_opacity=0.5)
vtkMeshView1 = VtkModel(b.vtk_window, patch_color=[[0,0.5,0.8]],mesh_opacity=0.8, force_wireframe=True)

# visualisation downsamplig
downsample = 10
while b.vtk_window.is_active:
    if q is not None:
        # construct an array with all the imu transformation matrices
        # - base
        # - biceps
        # - forearm
        # - hand
        wit_data = np.hstack(((R.from_quat(q[3])).as_matrix()[:3,:3].T.flatten(),
                            (R.from_quat(q[0])).as_matrix()[:3,:3].T.flatten(),
                            (R.from_quat(q[1])).as_matrix()[:3,:3].T.flatten(),
                            (R.from_quat(q[2])).as_matrix()[:3,:3].T.flatten()))
        
        # run the IK using the kalman filter
        kalman.reconstructFrame(model, wit_data, Q, Qdot, Qddot)
        
        # normalise angles between -pi and pi
        Q = normalize(Q.to_array(), Q_min, Q_max)
        Q = biorbd.GeneralizedCoordinates(Q)
        
        # downsample the visualisation
        downsample = downsample + 1
        if downsample > 50:

            model.updateMuscles(Q, True)
            model.UpdateKinematicsCustom(Q, np.zeros(nq), np.zeros(nq))

            F_max = []
            F_min = []
            for i in range(nb_mus):
                F_max.append(model.muscle(i).characteristics().forceIsoMax())
                #F_min.append(0)
                a = biorbd.HillThelenType(model.muscle(i)).FlPE()
                if a > 1:
                    a = 0.1
                elif a < 0:
                    a = 0
                F_min.append(a*F_max[-1])

            start = time.time()
            N = -model.musclesLengthJacobian(Q).to_array().T
            J = model.markersJacobian(Q, False, False)[-1].to_array()
            print("matrices time", time.time() - start)

            # Proceed with the inverse dynamics
            Tau_grav = model.InverseDynamics(Q, np.zeros(nq), np.zeros(nq))

            # remove the base joint
            J = J[:,1:]
            N = N[1:,:]
            tau_b = -Tau_grav.to_array()[1:]

            # remove the wrist as it does not participate much in the force generation
            J = J[:,:-2]
            N = N[:-2,:]
            tau_b = tau_b[:-2]

            
            start = time.time()
            f_poly = pycapacity.human.force_polytope(J, N, F_min, F_max, 10, tau_b)
            print("polytope time", time.time() - start)

            ## display polytope in the bioviz
            f_vert_show = f_poly.vertices/2000 + model.markers(Q)[model.nbMarkers()-1].to_array()[:,None]

            # plot polytope (blue) - with the edges
            meshes = []
            meshes.append(Mesh(vertex=f_vert_show[:,:,None], triangles=f_poly.face_indices.T))
            vtkMeshView.new_mesh_set(meshes)
            vtkMeshView1.new_mesh_set(meshes)

            b.set_q(Q.to_array())
            b.update()
            downsample = 0 
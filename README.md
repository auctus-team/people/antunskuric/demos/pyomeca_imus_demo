# A demo repository for right arm inverse kinematics using IMUs

This repository is based on biomechanics library `pyomeca` [biorbd](https://github.com/pyomeca/biorbd) and visualisation using `pyomeca` [bioviz](https://github.com/pyomeca/bioviz).

The inverse kinematics is done using IMUs. 



##  Installation 

Using anaconda, you can install the dependencies using the following command:

```bash
conda env create -f env.yaml
```
Then install the witmotion IMU python library [pywitmotion](https://github.com/askuric/pywitmotion)

```bash
pip install git+https://github.com/askuric/pywitmotion.git
```

To run the demo you'll need to install the bluetooth library `pybluez` to be able to connect to the IMU via bluetooth.

```bash
pip install pybluez
```
Make sure to pair the IMUs with your computer before running the demo.

If they do not want to pair, you might have to install `blueman` package on linux. 
```sh
sudo apt-get install blueman
```
Then use the `blueman` gui to pair the IMUs.
```sh
blueman-manager
```

## Witmotion IMUs - BWT901CL 
<details><summary>Read more</summary>
<img width="400px" src="https://github.com/askuric/pywitmotion/raw/main/datasheet/image.jpg">

This is a 9-axis IMU with a bluetooth connection. 
Its a very cool device that can be used for many applications. It is completely standalone and has integrated battery, accelerometer, gyroscope, magnetometer and barometer. So it can give you an absolute orientation in space. 

The sensors are relatively cheap and can be bought on Amazon [link](https://www.amazon.fr/gp/product/B01MYRVC1H/ref=ppx_yo_dt_b_asin_title_o06_s00?ie=UTF8&psc=1) - price around 50€. There are many of them out there, using BLE, Wifi, USB, etc. 
The one I've found the most useful is the BWT901CL, which is bluetooth based and is 9dof. 

The documentation is not great but the device is easy enough to use.
Here is a link to the datasheet [BWT901CL Datasheet.pdf](https://github.com/askuric/pywitmotion/blob/main/datasheet/BWT901_Datasheet.pdf). 
There is an android application that can be used to parametrize the sensor. 

Once you download the app:
- Pair the sensor with tha app
- Calibrate the sensor (magnetometer, accelerometer and gyro) 
- Set the desired bandwidth (number of samples that are sent per second) (50-100Hz is good enough for most applications)
- Make sure to use 9dof mode not 6dof 
- Enable only the quaternion data and disable the other ones  (accelerometer, gyroscope, magnetometer, euler angles, etc.)

</details>


## Musculoskeletal model
<details><summary>Read more</summary>
<img width="600px" src="images/model.png">

The model used in the demo is the model from Holzbaur et al. 2005 [link](https://pubmed.ncbi.nlm.nih.gov/16078622/) with 50 muscles and 7 joints. The model is extended with an additional degree of freedom - rotation around the waist.

</details>

## Placing IMUs on the body
<details><summary>Read more</summary>
<img width="600px" src="images/sensor_position.jpg">

Positioning the sensors is very easy, just make sure to repsect the order:
1) Biceps
2) Forearm
3) Hand
4) Waist - Lower Bask

Make sure when your arm is extended downwards, all the IMUs have the y-axis pointing upwards.

The IMU on the back should have the y-axis pointing upwards as well.
</details>


## Running the demo

First activate the conda environment:

```bash
conda activate demo_imus
```

###  IMUs testing script
Then run the IMU test script to make sure that the IMUs are connected correctly:

```bash 
conda activate demo_imus # if not already activated
python imus_connection_test.py
```
If everything is working correctly, you should see the successful connection to the imus:
```sh
Device 0 - 00:0C:BF:16:71:33 connected!
Device 1 - 00:0C:BF:09:46:4B connected!
Device 2 - 00:0C:BF:02:1E:40 connected!
Device 3 - 00:0C:BF:02:3B:42 connected!
```
And then each second you should receive the quaternion data from each IMU:
```sh
Quaternions:
[array([ 0.22375488, -0.52929688, -0.6383667 ,  0.51199341]), 
array([-0.04193115, -0.28662109, -0.62399292,  0.72570801]), 
array([-0.30834961,  0.12435913, -0.74984741,  0.57189941]), 
array([0.65887451, 0.63876343, 0.32226562, 0.23220825])]
```


### Inverse kinematics demo
When this is running correctly, you can run the inverse kinematics demo.

To run the demo you can either use the Python script:

```bash
conda activate demo_imus # if not already activated
python pyomeca_ik_imus.py
```
or use jupyter notebook:
```bash
conda activate demo_imus # if not already activated
jupyter lab
```

If everything is working correctly, you should see the following window:

<img src="images/demo_window.png" width="500px">

And you should be able to move the arm and see the model moving accordingly.

<img src="images/pyomeca_imus.gif" width="1000px">


### Demo with polytopes

To run the demo with pyomeca IK with polytopes use the Python script:

```bash
conda activate demo_imus # if not already activated
python pyomeca_ik_imus_withpolytopes.py
```


And you should be able to move the arm and see the model moving accordingly.

<img src="images/pyomeca_imus_polytope.gif" width="1000px">